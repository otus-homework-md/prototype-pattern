﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prototype.Model.Interfaces
{
    [Flags]
    enum RealtyType
    {
        commercial=0,
        privateFlat
    }
    internal interface IRealty
    {
        RealtyType RealtyType { get; set; }

    }
}
