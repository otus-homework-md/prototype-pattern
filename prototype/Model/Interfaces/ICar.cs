﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prototype.Model.Interfaces
{
    internal interface ICar
    {
        string Name { get; set; }
        string Color { get; set; }
    }
}
