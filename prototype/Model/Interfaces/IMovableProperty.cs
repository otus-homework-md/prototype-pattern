﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prototype.Model.Interfaces
{
    [Flags]
    enum MoveblePropertyType
    {
        car = 1,
        airplain,
        ship
    }
    internal interface IMovableProperty
    {
        MoveblePropertyType MoveblePropertyType { get; set; }
    }
}
