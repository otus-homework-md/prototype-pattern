﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prototype.Model.Interfaces
{
    internal interface IProperty
    {
        string Owner { get; set; }
    }
}
