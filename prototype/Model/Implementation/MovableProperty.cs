﻿using prototype.Infrastructure;
using prototype.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prototype.Model.Implementation
{
    internal class MovableProperty :Property, IMovableProperty, IMyCloneable<IMovableProperty>
    {
        public MoveblePropertyType MoveblePropertyType { get; set ; }

        public MovableProperty(string Owner="Mike", MoveblePropertyType moveblePropertyType=MoveblePropertyType.car):base(Owner)
        {
            MoveblePropertyType = moveblePropertyType;
        }

        public new IMovableProperty Copy()
        {
            return new MovableProperty(this.Owner, this.MoveblePropertyType);
        }

        public override string ToString()
        {
            return base.ToString()+ " - "+MoveblePropertyType ;
        }

        public new object Clone() { return Copy(); }
    }
}
