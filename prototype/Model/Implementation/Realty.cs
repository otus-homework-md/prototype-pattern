﻿using prototype.Infrastructure;
using prototype.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prototype.Model.Implementation
{
    internal class Realty : Property, IRealty, IMyCloneable<IRealty>
    {
        public RealtyType RealtyType { get ; set; }

        public Realty(string ownerName="Mike",RealtyType realtyType=RealtyType.privateFlat) : base(ownerName) 
        { 
            RealtyType = realtyType;
        }

        public new IRealty Copy()
        {
            return new Realty(this.Owner,this.RealtyType);
        }

        public override string ToString()
        {
            return base.ToString() +" - "+RealtyType;
        }

        public new object Clone() { return Copy(); }

        
    }
}
