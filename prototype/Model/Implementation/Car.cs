﻿using prototype.Infrastructure;
using prototype.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prototype.Model.Implementation
{
    internal class Car : MovableProperty, ICar, IMyCloneable<ICar>
    {
        public string Name { get; set; }
        public string Color { get ; set; }

        public Car(string owner="Mike", string name="Mitsubishi Lancer", string color="Red"):base(owner,MoveblePropertyType.car)
        {
            Name = name;
            Color = color;
        }

        public new ICar Copy()
        {
            return new Car(this.Owner, this.Name, this.Color);
        }

        public override string ToString()
        {
            return base.ToString()+" - " +Name+ " - "+ Color ;
        }

        public new object Clone() { return Copy(); }
    }
}
