﻿using prototype.Infrastructure;
using prototype.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prototype.Model.Implementation
{
    internal class Property : IProperty, IMyCloneable<IProperty>,ICloneable
    {
        public string Owner { get; set; }

        public Guid Id { get; set; }

        public Property(string owner="Mike")
        {
            Owner = owner;
            Id = Guid.NewGuid();
        }

        public IProperty Copy()
        {
            return new Property(Owner);
        }

        public override string ToString()
        {
            return Id.ToString()+ " Property of "+Owner.ToString();
        }

        public object Clone()
        {            
                return Copy();            
        }
    }
}
