﻿using prototype.Model.Implementation;
using prototype.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prototype.Infrastructure
{
    internal class Processor : IProcessor
    {
        public void Process()
        {
            Console.WriteLine("Create base class");
            Property property=new Property("Kirill Ivanov");
            Console.WriteLine(property);
            Console.WriteLine("MyClone - " + property.Copy());
            Console.WriteLine("Clone - "+ (IProperty)property.Clone());
            
            Console.WriteLine("--------------------------");
            Console.WriteLine("Create base class");
            Realty realty = new Realty("Ivan Ivanov",RealtyType.privateFlat);
            Console.WriteLine(realty);
            Console.WriteLine("MyClone - " + realty.Copy());
            Console.WriteLine("Clone - " + (IRealty)realty.Clone());

            Console.WriteLine("--------------------------");
            Console.WriteLine("Create base class");
            MovableProperty movableproperty = new MovableProperty("Ivan Sidorov");
            Console.WriteLine(movableproperty);
            Console.WriteLine("MyClone - " + movableproperty.Copy());
            Console.WriteLine("Clone - " + (IMovableProperty)movableproperty.Clone());

            Console.WriteLine("--------------------------");
            Console.WriteLine("Create base class");
            Car car = new Car("Peter Sidorov");
            Console.WriteLine(car);
            Console.WriteLine("MyClone - " + car.Copy());
            Console.WriteLine("Clone - " + (IMovableProperty)car.Clone());
        }
    }
}
