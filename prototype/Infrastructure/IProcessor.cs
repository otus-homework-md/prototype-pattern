﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prototype.Infrastructure
{
    internal interface IProcessor
    {
        void Process();
    }
}
